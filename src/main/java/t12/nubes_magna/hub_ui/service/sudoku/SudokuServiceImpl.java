package t12.nubes_magna.hub_ui.service.sudoku;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import t12.nubes_magna.hub_ui.model.sudoku.DifficultyLevel;
import t12.nubes_magna.hub_ui.model.sudoku.Grid;

import java.util.List;

@Service
public class SudokuServiceImpl implements SudokuService {

    public static final String SUDOKU_SERVICE_NAME = "sudoku-service";

    @Autowired
    private DiscoveryClient discoveryClient;

    @Autowired
    @LoadBalanced
    private RestTemplate loadBalanced;

    @Override
    public boolean isSudokuExternalServiceAvailable() {
        final List<ServiceInstance> instances = discoveryClient.getInstances(SUDOKU_SERVICE_NAME);
        return instances != null && !instances.isEmpty();
    }

    @Override
    public Grid getGridForDifficulty(String difficultyLevel) {

        final ResponseEntity<Grid> responseEntity = loadBalanced
                .getForEntity("http://" + SUDOKU_SERVICE_NAME + "/sudoku/generate?difficulty={difficulty}", Grid.class,
                        difficultyLevel);
        final Grid grid = responseEntity.getBody();
        return grid;
    }

    @Override
    public DifficultyLevel[] getDifficultyLevels() {

        final ResponseEntity<DifficultyLevel[]> responseEntity = loadBalanced
                .getForEntity("http://" + SUDOKU_SERVICE_NAME + "/sudoku/difficulty", DifficultyLevel[].class);

        final DifficultyLevel[] difficultyLevels = responseEntity.getBody();
        return difficultyLevels;
    }

    @Override
    public Grid solve(final String stringRepresentation) {

        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.TEXT_PLAIN);
        final HttpEntity<String> request = new HttpEntity<>(stringRepresentation, headers);

        final String url = "http://" + SUDOKU_SERVICE_NAME + "/sudoku/solve";
        Grid grid = loadBalanced.postForObject(url, request, Grid.class);

        return grid;
    }

    @Override
    public boolean checkSolution(final String stringRepresentation) {

        final HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.TEXT_PLAIN);
        final HttpEntity<String> request = new HttpEntity<>(stringRepresentation, headers);

        final String url = "http://" + SUDOKU_SERVICE_NAME + "/sudoku/checkSolution";
        boolean grid = loadBalanced.postForObject(url, request, Boolean.class);

        return grid;
    }
}
