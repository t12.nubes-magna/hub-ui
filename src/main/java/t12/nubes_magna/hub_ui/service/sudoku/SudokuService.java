package t12.nubes_magna.hub_ui.service.sudoku;


import t12.nubes_magna.hub_ui.model.sudoku.DifficultyLevel;
import t12.nubes_magna.hub_ui.model.sudoku.Grid;

public interface SudokuService {

    boolean isSudokuExternalServiceAvailable();

    Grid getGridForDifficulty(final String difficultyLevel);

    DifficultyLevel[] getDifficultyLevels();

    Grid solve(final String stringRepresentation);

    boolean checkSolution(final String stringRepresentation);
}
