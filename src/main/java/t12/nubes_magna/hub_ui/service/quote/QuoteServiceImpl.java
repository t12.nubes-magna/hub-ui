package t12.nubes_magna.hub_ui.service.quote;


import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import t12.nubes_magna.hub_ui.model.quote.Quote;

@Service
public class QuoteServiceImpl implements QuoteService {

    public static final String QUOTE_SERVICE_NAME = "QUOTE-SERVICE";

    @Autowired
    @LoadBalanced
    private RestTemplate loadBalanced;

    @Override
    @HystrixCommand(fallbackMethod = "fallback")
    public Quote getQuoteOfTheDay() {
        final ResponseEntity<Quote> responseEntity = loadBalanced
                .getForEntity("http://" + QUOTE_SERVICE_NAME + "/quotes/quoteOfDay", Quote.class);
        final Quote quote = responseEntity.getBody();
        return quote;
    }

    public Quote fallback() {
        return new Quote.QuoteBuilder()
                .setQuote("The thing about quotes on the internet is you ca not confirm their validity.")
                .setAuthor("Abraham Lincoln").createQuote();
    }
}
