package t12.nubes_magna.hub_ui.service.quote;

import t12.nubes_magna.hub_ui.model.quote.Quote;

public interface QuoteService {
    Quote getQuoteOfTheDay();
}
