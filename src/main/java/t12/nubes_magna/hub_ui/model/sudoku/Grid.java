package t12.nubes_magna.hub_ui.model.sudoku;

import java.io.Serializable;

public class Grid implements Serializable {

    private static final long serialVersionUID = 8249578441380396469L;

    private int[][] cells;
    private boolean solved = false;

    public Grid() {
    }

    public int[][] getCells() {
        return cells;
    }

    public void setCells(int[][] cells) {
        this.cells = cells;
    }

    public boolean isSolved() {
        return solved;
    }

    public void setSolved(boolean solved) {
        this.solved = solved;
    }
}
