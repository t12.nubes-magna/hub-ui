package t12.nubes_magna.hub_ui.model.sudoku;

import java.io.Serializable;

public class DifficultyLevel implements Serializable {

    private static final long serialVersionUID = 23544521475631129L;

    private String level;
    private int score;

    public DifficultyLevel() {
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }
}
