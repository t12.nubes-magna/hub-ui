package t12.nubes_magna.hub_ui.model.quote;

import java.io.Serializable;

public class Quote implements Serializable {

    private static final long serialVersionUID = -3087826741683062815L;
    private Integer id;
    private String quote;
    private String author;
    private String submitter;

    public Quote(final Integer id, final String quote, final String author, final String submitter) {
        this.id = id;
        this.quote = quote;
        this.author = author;
        this.submitter = submitter;
    }

    private Quote() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getQuote() {
        return quote;
    }

    public void setQuote(final String quote) {
        this.quote = quote;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(final String author) {
        this.author = author;
    }

    public String getSubmitter() {
        return submitter;
    }

    public void setSubmitter(final String submitter) {
        this.submitter = submitter;
    }

    public static class QuoteBuilder {

        private Integer id;
        private String quote;
        private String author;
        private String submitter;

        public QuoteBuilder setId(final Integer id) {
            this.id = id;
            return this;
        }

        public QuoteBuilder setQuote(final String quote) {
            this.quote = quote;
            return this;
        }

        public QuoteBuilder setAuthor(final String author) {
            this.author = author;
            return this;
        }

        public QuoteBuilder setSubmitter(final String submitter) {
            this.submitter = submitter;
            return this;
        }

        public Quote createQuote() {
            return new Quote(id, quote, author, submitter);
        }
    }
}


