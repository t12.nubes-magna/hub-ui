package t12.nubes_magna.hub_ui.controller.rest;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import t12.nubes_magna.hub_ui.model.sudoku.Grid;
import t12.nubes_magna.hub_ui.service.sudoku.SudokuService;

@RestController
@RequestMapping("/api/sudoku")
public class SudokuAPI {

    @Autowired
    SudokuService sudokuService;

    @GetMapping("/generate")
    public Grid generate(@RequestParam(name = "difficulty", defaultValue = "MEDIUM") String difficultyLevel) {

        return sudokuService.getGridForDifficulty(difficultyLevel);
    }

    @PostMapping("/solve")
    public Grid solve(@RequestBody String stringRepresentation) {

        return sudokuService.solve(stringRepresentation);
    }

    @PostMapping("/check")
    public boolean checkSolution(@RequestBody String stringRepresentation) {

        return sudokuService.checkSolution(stringRepresentation);
    }
}
