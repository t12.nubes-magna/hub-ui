package t12.nubes_magna.hub_ui.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import t12.nubes_magna.hub_ui.model.sudoku.DifficultyLevel;
import t12.nubes_magna.hub_ui.service.ConfigurationService;
import t12.nubes_magna.hub_ui.service.quote.QuoteService;
import t12.nubes_magna.hub_ui.service.sudoku.SudokuService;

import java.util.Map;

@RefreshScope
@Controller
public class MainController {

    @Autowired
    private ConfigurationService configurationService;

    @Autowired
    private SudokuService sudokuServiceImpl;

    @Autowired
    private QuoteService quoteServiceImpl;

    @GetMapping({"/", "/index", "/hub"})
    public String index(Map<String, Object> model) {
        model.put("isSudokuAvailable", sudokuServiceImpl.isSudokuExternalServiceAvailable());
        model.put("quoteOfTheDay", quoteServiceImpl.getQuoteOfTheDay());
        model.put("welcomeMessage", configurationService.getWelcomeMessage());
        return "index";
    }

    @GetMapping({"/hub/sudoku"})
    public String sudoku(Map<String, Object> model) {

        final DifficultyLevel[] difficultyLevels = sudokuServiceImpl.getDifficultyLevels();
        model.put("difficultyLevels", difficultyLevels);

        return "sudoku/sudoku";
    }
}
