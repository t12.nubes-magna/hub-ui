function generateGrid(grid) {

    var html = '';
    html += '<tbody>';

    for (var row = 0; row < 9; ++row) {
        html += '<tr>';
        for (var column = 0; column < 9; ++column) {
            html += '<td>';
            var id = row + '-' + column;
            html += '<input type=\'text\' id=\'cell-' + id + '\' maxlength=\'1\' ';
            if (grid) {
                var cell = grid[row][column];
                if (cell !== 0) {
                    html += ' disabled ';
                    html += ' value=\'' + cell + '\'';
                } else {
                    html += ' value=\'\'';
                }
            } else {
                html += ' value=\'\'';
            }

            html += '/>';
            html += '</td>';
        }
        html += '</tr>';
    }
    html += '</tbody>';

    $('#sudoku-grid').empty().append(html);
}

function generateGridWithDifficulty(difficultyLevel) {
    if (difficultyLevel) {
        $.ajax({
            dataType: 'JSON',
            url: '/api/sudoku/generate?difficulty=' + difficultyLevel,
            method: 'GET',
            success: function (data) {
                generateGrid(data.cells);
            }
        });
    }
}

function solveSudoku() {


    $.ajax({
        contentType: 'text/plain',
        dataType: 'JSON',
        url: '/api/sudoku/solve',
        data: getStringRepresentationOfGrid(),
        method: 'POST',
        success: function (data) {
            if (!data || data.solved) {
                for (var row = 0; row < 9; ++row) {
                    for (var column = 0; column < 9; ++column) {
                        var cell = $('#cell-' + row + '-' + column);

                        if (!cell.attr('disabled')) {
                            cell.val(data.cells[row][column]);
                        }
                    }
                }
            } else {
                alert("Couldn't solve puzzle");
            }
        }
    });

}
function checkIfValidSolution() {
    $.ajax({
        contentType: 'text/plain',
        dataType: 'JSON',
        url: '/api/sudoku/check',
        data: getStringRepresentationOfGrid(),
        method: 'POST',
        success: function (data) {
            if (data) {
                alert("Congrats! It is complete and valid.");
            } else {
                alert("Oh, no. You fucked up somewhere.");
            }
        }
    });

}

function getStringRepresentationOfGrid() {
    var cells = '';
    for (var row = 0; row < 9; ++row) {
        for (var column = 0; column < 9; ++column) {
            var value = $('#cell-' + row + '-' + column).val();
            if (!value) {
                value = 0;
            }
            cells += value;
            if (row != 8 || column != 8) {
                cells += ",";
            }
        }
    }
    return cells;
}
$(function () {

    generateGrid(null);

});